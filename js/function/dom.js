﻿import { TodoList,TodoListItem } from "../components/todolist.js";

/**
 * 
 * @param {string} tagname 
 * @param {object} attribute 
 * @returns {HTMLElement}
 */
export function createElement(tagname,attributes = {}){
    const element = document.createElement(tagname);
    for ( const [attribute,value] of Object.entries(attributes)){
        if (value !== null ){            
        element.setAttribute(attribute,value);
        }
    }
    return element;
}

/**
 * 
 * @param {string} Event 
 * @param {HTMLElement} element 
 */
export function createEventElement(event,element){
    const form = element.querySelector('form');
    form.addEventListener('submit', e => onsubmit(e));
}

/**
 * 
 * @param {SubmitEvent} e 
 */
 function onsubmit(e){
    e.preventDefault();
    const form = e.currentTarget;
    const valueNewTodo = form.querySelector('input').value;
    const title = valueNewTodo.trim();
    if (title === ''){
        return
    }
    const todo = {
        id : Date.now(),
        title,
        completed : false
    }

    const item = new TodoListItem(todo);
    const listTodos = document.querySelector("#list_todolist");
    item.appendTo(listTodos);      
    form.reset();
}
