﻿import { createElement } from "../function/dom.js";

/**
 * @typedef {object} Todo
 * @property {number} id
 * @property {string} title
 * @property {boolean} completed
 */
export class TodoList {

    /** @type  {Todo[]} */
    #todos = [];

    /**
     * 
     * @param {Todo[]} todos 
     */
    constructor(todos) {
        this.#todos = todos
    }

    /**
     * 
     * @param {HTMLElement} element 
     */
    appendTo(element) {
        const list = element.querySelector('#list_todolist');
        for (let todo of this.#todos) {
            const currentTodo = new TodoListItem(todo);
            currentTodo.appendTo(list);
        }
    }
}

export class TodoListItem {

    #element;
    /**
     * 
     * @param {Todo} todo 
     */
    constructor(todo) {
        const id = todo.id;
        const li = createElement('li', {
            class: 'flex items-center justify-between border border-black-500 rounded-md p-2'
        });
        const input = createElement('input', {
            type: "checkbox",
            name: "todo_" + id,
            id: "todo_" + id,
            checked: todo.completed ? '' : null
        });
        const label = createElement('label', {
            class: '"mx-2 p-2 w-full',
            for: "todo_" + id
        });
        label.innerHTML = todo.title;
        const button = createElement('button', {
            class: 'remove p-2 bg-red-700 text-white rounded-sm',
            type: "submit"
        });
        button.innerHTML = `<svg
        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
        stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round"
            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
    </svg>`;
        li.append(input);
        li.append(label);
        li.append(button);
        button.addEventListener('click', e => this.remove(e));
        input.addEventListener('change', e => this.changeChecked(e));
        this.#element = li;
    }

    /**
     * 
     * @param {HTMLElement} element 
     */
    appendTo(element) {
        const li = this.#element;
        const groupefilter = document.querySelector('#container_filter');
        const filter = groupefilter.querySelector('li.active').dataset.filter;    
        (filter === "todo" || filter === "nottodo" ? li.classList.add('hidden') : li.classList.remove('hidden')); 
        element.prepend(this.#element);
    }

    /**
     * 
     * @param {PointerEvent} e 
     */
    remove(e) {
        e.preventDefault();
        this.#element.remove();
    }

    changeChecked(e) {
        e.preventDefault();
        const li = e.currentTarget.parentElement;
        const groupefilter = document.querySelector('#container_filter');
        const filter = groupefilter.querySelector('li.active').dataset.filter;
        (filter === "todo" || filter === "nottodo" ? li.classList.add('hidden') : li.classList.remove('hidden'));
    }


}


