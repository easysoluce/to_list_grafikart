﻿


export class FilterList {

    #groupeFilter;

    /**
     * 
     * @param {string} groupeNameFilter 
     */
    constructor(groupeNameFilter) {
        this.#groupeFilter = document.querySelector(groupeNameFilter);
     
    }


    /**
     * 
     * @param {string} tag 
     * @param {PointerEvent} event
     */
    createEvent(tag, event) {
        const groupeElement = this.#groupeFilter;
        const listItemFilter = groupeElement.querySelectorAll(tag);
        for (let itemFilter of listItemFilter) {
            itemFilter.addEventListener(event, e => this.clickItemFilter(e));
        }
    }

    /**
     * 
     * @param {event} e 
     */
    clickItemFilter(e) {
        e.preventDefault();
        const li = e.currentTarget;
        const groupeFilter = document.querySelector('#container_filter');
        const currentFilter = groupeFilter.querySelector('li.active');
        currentFilter.classList.remove('active');
        li.classList.add('active');
        const datasetFilter = e.currentTarget.dataset.filter;
        const groupTodoList = document.querySelector('#container_todolist');
        const listItemTodoList = groupTodoList.querySelectorAll('input');
        for (let input of listItemTodoList) {
            this.changedVisible(datasetFilter, input);
        }
    }

    changedVisible(filter, inputElement) {
        const liElement = inputElement.parentElement;
        liElement.classList.remove('hidden');
        (!inputElement.checked && filter === "todo" ? liElement.classList.add('hidden') : null);
        (inputElement.checked && filter === "nottodo" ? liElement.classList.add('hidden') : null);
    }
}



