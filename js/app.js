﻿import { TodoList } from "./components/todolist.js";
import { fecthJSON } from "./function/api.js";
import { createElement, createEventElement } from "./function/dom.js";
import { FilterList } from "./components/filtertodo.js"

try {
    const todos = await fecthJSON('https://jsonplaceholder.typicode.com/todos?_limit=5');
    const list = new TodoList(todos);
    list.appendTo(document.querySelector('#container_todolist'));
    const addlist = document.querySelector('#container_search');
    createEventElement("submit", addlist);
    // Gestion event click filter
    const groupeListFilter = new FilterList("#container_filter");
    groupeListFilter.createEvent('li',"click");
} catch (e) {
    console.log(e.message);
    const alertElement = createElement('div', {
        class: 'flex justify-center font-bold p-2 rounded-md text-white mx-auto my-10 bg-red-50 text-red-400',
        role: 'alert'
    });
    alertElement.innerText = "Impossible de charger les éléments !";
    document.body.prepend(alertElement);
}





